<?php

return [
    'env' => 'dev',
    'app_debug' => true,
    'pwd_salt' => 'SVICK_SALT',
    'encryption' => [
        'aes' => [
            'key' => 'SVICK_AES_KEY',
            'method' => 'AES-128-CBC'
        ],
    ],
];
