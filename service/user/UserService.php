<?php
/**
 * UserService.php
 * User: tommy
 */

namespace service\user;

use lib\token\TokenLib;
use model\user\User;

class UserService
{
    /**
     * 登录
     *
     * @param $uid
     * @return array
     */
    public static function login($uid): array
    {
        $expire = 86400 * 7;
        $token = TokenLib::create($uid, $expire);

        User::where($uid)->update(['last_login_time' => NOW_TIME]);
        return [$token, time() + $expire];
    }
}
