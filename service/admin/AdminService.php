<?php
/**
 * AdminService.php
 * User: tommy
 */

namespace service\admin;


use lib\token\TokenLib;
use model\admin\Admin;

class AdminService
{
    /**
     * 登录
     *
     * @param $aid
     * @param $type
     * @return array
     */
    public static function login($aid, $type): array
    {
        $expire = 86400 * 7;
        $token = TokenLib::create([$aid, $type], $expire);

        Admin::where($aid)->update(['last_login_time' => NOW_TIME]);
        return [$token, time() + $expire];
    }

    /**
     * 获取管理员姓名
     *
     * @param $aid
     * @return mixed|string
     */
    public static function name($aid)
    {
        $admin = Admin::find($aid, 'name');
        return !$admin->isEmpty() ? $admin->name : '';
    }
}
