SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE IF NOT EXISTS svick DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_general_ci;

USE svick;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL DEFAULT '' COMMENT '姓名',
  `nickname` varchar(16) NOT NULL DEFAULT '' COMMENT '昵称',
  `avatar` varchar(32) NOT NULL DEFAULT '' COMMENT '头像',
  `age` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '年龄',
  `money` double(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 -1删除 0禁用 1正常',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最近登录时间',
  `created` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间 ',
  `updated` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '刘禅', 'liushan', 'a/b/c/8973947883.jpg', '100', '0.00', '1', '1664251543', '1664250183', '0');
INSERT INTO `user` VALUES ('2', '赵云', 'zhaoyun', 'd/e/f/7837287494.jpg', '120', '0.00', '1', '0', '1664250183', '0');
INSERT INTO `user` VALUES ('3', '张飞', '', '', '80', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('4', '刘备', '', '', '68', '100.43', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('5', '关羽', '', '', '56', '98.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('6', '曹操', '', '', '81', '198.87', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('7', '吕布', '', '', '88', '87.87', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('8', '貂蝉', '', '', '65', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('9', '小乔', '', '', '45', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('10', '大乔', '', '', '60', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('11', '周瑜', '', '', '66', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('12', '典韦', '', '', '79', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('13', '马超', '', '', '70', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('14', '夏侯渊', '', '', '80', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('15', '黄忠', '', '', '90', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('16', '严颜', '', '', '35', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('17', '许褚', '', '', '45', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('18', '庞德', '', '', '55', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('19', '黄盖', '', '', '65', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('20', '诸葛亮', '', '', '39', '0.00', '1', '0', '0', '0');
INSERT INTO `user` VALUES ('21', '关平', '', '', '99', '99.90', '0', '0', '1667196581', '1667380907');
