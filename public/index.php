<?php

use Svick\Core\App;

define('ROOT_PATH', dirname(__DIR__) . DIRECTORY_SEPARATOR);

require ROOT_PATH . './vendor/autoload.php';

App::run('api');
