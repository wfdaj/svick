<?php

use Svick\Core\App;

define('ROOT_PATH', dirname(__DIR__, 2) . DIRECTORY_SEPARATOR);

require ROOT_PATH . './vendor/autoload.php';

App::run('admin');
