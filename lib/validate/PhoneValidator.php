<?php
/**
 * Svick a Fast Simple Smart PHP FrameWork
 * Author: Tommy 863758705@qq.com
 * Link: http://svick.tomener.com/
 * Since: 2022
 */

namespace lib\validate;

class PhoneValidator
{
    /**
     * 判断手机号码是否合法
     *
     * @param $phoneNumber
     * @return bool
     */
    public static function isValid($phoneNumber) {
        // 定义一个包含所有有效手机号码开头的数组
        $validPrefixes = [
            '13', '14', '15', '17', '18', '19',
            // 虚拟运营商号段
            '170', '171', '176', '177', '178',
            // 以及其他可能的新增号段，根据需要进行添加
        ];

        // 检查手机号码是否为11位数字
        if (!preg_match('/^\d{11}$/', $phoneNumber)) {
            return false;
        }

        // 检查手机号码的前缀是否在有效前缀列表中
        $prefix = substr($phoneNumber, 0, 2);
        if (!in_array($prefix, $validPrefixes)) {
            return false;
        }

        // 如果以上检查都通过，则手机号码格式正确
        return true;
    }
}
