<?php
/**
 * User: tommy
 */

namespace app\admin\controller;

use Exception;
use lib\qiniu\QiniuLib;
use stdClass;
use Svick\Core\App;

class Qiniu extends Base
{
    /**
     * 获取上传token
     *
     * @return stdClass
     * @throws Exception
     */
    public function token(): stdClass
    {
        $token = QiniuLib::getUpToken('tmp');
        return App::result(0, 'ok', [
            'token' => $token
        ]);
    }
}
