<?php
/**
 * Index.php
 * User: tommy
 */

namespace app\admin\controller;

use stdClass;
use Svick\Core\App;

class Index
{
    /**
     * @return stdClass
     */
    public function index()
    {
        return App::result(0, 'Welcome to Svick Framework', [
            'app' => 'admin',
            'version' => App::VERSION,
            'env' => App::$env
        ]);
    }
}
