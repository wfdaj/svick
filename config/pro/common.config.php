<?php
return [
    'project' => [
        'mode' => 0, //0单模块 1多模块 2多版本
    ],
    'http_content_type' => 'json',
    'cache' => [
        'type' => 'File',
        'sub_dir' => 'cache',
        'use_random_dir' => true,
        'path_level' => 2,
    ],
    'qiniu' => [
        'key' => [
            'ak' => '',
            'sk' => '',
        ],
        'bucket' => [
            'tmp' => 'b-tmp',
            'data' => 'b-data',
        ],
        'domain' => [
            'tmp' => 'http://tmp.cdn.svick.com/',
            'data' => 'http://data.cdn.svick.com/'
        ],
        'prefix' => '', //存储key的前缀，多项目时可使用，用于区分，如：blc/，为空表示不加前缀
    ],
    'wechat' => [
        'mp' => [ //公众号
            'appid' => '',
            'secret' => '',
        ],
        'ma' => [ //小程序 minapp
            'appid' => '',
            'secret' => '',
        ],
    ],
];
