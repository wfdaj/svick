<?php
/**
 * Svick a Fast Simple Smart PHP FrameWork
 * Author: Tommy 863758705@qq.com
 * Link: http://svick.tomener.com/
 * Since: 2022
 */

namespace lib\token;


use lib\encrypt\AesEncrypt;

class TokenLib
{
    public static function create($data, $expire)
    {
        if (!is_array($data)) {
            $data = [$data];
        }
        $data = json_encode($data, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $token = AesEncrypt::make()->expire($expire)->encrypt($data);
        return $token;
    }

    public static function validate($token)
    {
        $ret = AesEncrypt::make()->decrypt($token);
        if ($ret[0] === 0) {
            $ret[2] = json_decode($ret[2], true);
            if (count($ret[2]) == 1) {
                $ret[2] = $ret[2][0];
            }
        }
        return $ret;
    }
}
