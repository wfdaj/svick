<?php
/**
 * Index.php
 * User: tommy
 */

namespace app\api\controller;

use stdClass;
use Svick\Core\App;

class Index
{
    /**
     * @return stdClass
     */
    public function index()
    {
        return App::result(0, 'Welcome to Svick Framework', [
            'app' => APP_NAME,
            'version' => App::VERSION,
            'env' => App::$env
        ]);
    }
}
