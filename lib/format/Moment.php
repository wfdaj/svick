<?php
/**
 * Svick a Fast Simple Smart PHP FrameWork
 * Author: Tommy 863758705@qq.com
 * Link: http://svick.tomener.com/
 * Since: 2022
 */

namespace lib\format;

class Moment
{
    public static function now()
    {
        return NOW_TIME;
    }

    /**
     * 格式化时间
     *
     * @param $format
     * @param $timestamp int 假的时间戳
     * @return false|string
     */
    public static function date($format, $timestamp = -1)
    {
        if ($timestamp < 0) {
            $timestamp = Moment::now();
        }
        return date($format, $timestamp);
    }

    /**
     * 格式化简短显示日期时间
     *
     * @param $timestamp
     * @return string
     */
    public static function dateShow($timestamp): string
    {
        return $timestamp > 0 ? date('Y-m-d', $timestamp) : '';
    }

    /**
     * 格式化简短显示日期时间
     *
     * @param $timestamp
     * @return string
     */
    public static function dateYnjShow($timestamp): string
    {
        return $timestamp > 0 ? date('Y-m-d', $timestamp) : '';
    }

    /**
     * 格式化简短显示日期时间
     *
     * @param $timestamp
     * @return string
     */
    public static function dateHiShow($timestamp): string
    {
        return $timestamp > 0 ? date('Y-m-d H:i', $timestamp) : '';
    }

    /**
     * 格式化显示完整日期时间
     *
     * @param $timestamp
     * @return string
     */
    public static function datetimeShow($timestamp): string
    {
        return $timestamp > 0 ? date('Y-m-d H:i:s', $timestamp) : '';
    }

    /**
     * 某天零点时间戳
     *
     * 七天之前daysTime(-7)
     * 十天之后daysTime(10)
     *
     * @param $days
     * @return int
     */
    public static function daysFirstTime($days): int
    {
        return strtotime(date('Y-m-d', strtotime($days . ' days')));
    }

    /**
     * 某天最晚时间戳 23:59:59
     *
     * @param $days
     * @return false|int
     */
    public static function daysLatestTime($days)
    {
        return strtotime(date('Y-m-d 23:59:59', strtotime($days . ' days')));
    }

    /**
     * 当天0点时间戳
     *
     * @param $timestamp
     * @return false|int
     */
    public static function dayFirstTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        return strtotime(date('Y-m-d', $timestamp));
    }

    /**
     * 当天23:59:59时间戳
     *
     * @param $timestamp
     * @return false|int
     */
    public static function dayLatestTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        return strtotime(date('Y-m-d 23:59:59', $timestamp));
    }

    /**
     * 前一天凌晨0点时间戳
     *
     * @param $timestamp
     * @return false|int
     */
    public static function dayBeforeFirstTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        return strtotime('yesterday', $timestamp);
    }

    /**
     * 前一天23:59:59时间戳
     *
     * @param $timestamp
     * @return false|int
     */
    public static function dayBeforeLatestTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        $yesterday = strtotime('yesterday', $timestamp);
        return strtotime(date('Y-m-d 23:59:59', $yesterday));
    }

    /**
     * 本周一零点时间戳
     *
     * @return int
     */
    public static function weekFirstTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        return mktime(0, 0, 0, date('m', $timestamp), intval(date('d', $timestamp)) - date('N', $timestamp) + 1, date('Y', $timestamp));
    }

    /**
     * 本周日最晚时间戳
     *
     * @return int
     */
    public static function weekLatestTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        return mktime(23, 59, 59, date('m', $timestamp), intval(date('d', $timestamp)) - date('N', $timestamp) + 7, date('Y', $timestamp));
    }

    /**
     * 本月第一天时间戳
     *
     * @param $timestamp
     * @return int
     */
    public static function monthFirstTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        return mktime(0, 0, 0, date('m', $timestamp), 1, date('Y', $timestamp));
    }

    /**
     * 本月最后一天时间戳
     *
     * @param $timestamp
     * @return int
     */
    public static function monthLatestTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        return mktime(23, 59, 59, date('m', $timestamp), date('t', $timestamp), date('Y', $timestamp));
    }

    /**
     * 30天之前那天零点时间戳
     *
     * @return int
     */
    public static function oneMonthAgoTime()
    {
        return strtotime(date('Y-m-d', strtotime('-30 days')));
    }

    /**
     * 本季度第一天时间戳
     *
     * @param $timestamp
     * @return int
     */
    public static function seasonFirstTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        $season = ceil(date('m', $timestamp) / 3);
        return mktime(0, 0, 0, $season * 3 - 2, 1, date('Y', $timestamp));
    }

    /**
     * 本季度最后一天时间戳
     *
     * @param $timestamp
     * @return int
     */
    public static function seasonLatestTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        $season = ceil((date('m', $timestamp)) / 3);
        return mktime(0, 0, 0, $season * 3 + 1, 1, date('Y', $timestamp)) - 1;
    }

    public static function yearFirstTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        return mktime(0, 0, 0, 1, 1, date('Y', $timestamp));
    }

    public static function yearLatestTime($timestamp = null)
    {
        $timestamp = $timestamp == null ? time() : $timestamp;
        return mktime(23, 59, 59, 12, 31, date('Y', $timestamp));
    }
}
