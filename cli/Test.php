<?php
/**
 * User.php
 * User: tommy
 */

namespace cli;

use lib\password\PasswordLib;
use lib\token\TokenLib;
use model\admin\Admin;

class Test
{
    /**
     * 执行：php svick test:t
     */
    public function t()
    {
        echo 'test';
    }

    /**
     * 生成测试Token
     *
     * @param $uid
     */
    public function token($uid)
    {
        $token = TokenLib::create($uid, 0);
        echo $token;
    }

    public function pwd($uid, $pwd)
    {
        $password = PasswordLib::gen($pwd);

        Admin::where($uid)->update(['password' => $password]);
    }
}
